
import Foundation
import Foundation
class myApi {
    static func postApi(withURLRequest urlRequest: URLRequest, callback: @escaping ((Error?, [String: Any]?) -> Void)) {
        var request = urlRequest
        request.httpMethod = "POST"
let urlSession = URLSession(configuration: .default)
        dataTaskHandling(requested : request,session : urlSession,callback: callback)
        
    }
    
    
func getApi(withURLRequest urlRequest: URLRequest, callback: @escaping ((Error?, [String: Any]?) -> Void)) {
        var request = urlRequest
        request.httpMethod = "GET"
    
    let urlSession = URLSession(configuration: .default)
    myApi.dataTaskHandling(requested : request,session : urlSession,callback: callback)
}
    static func dataTaskHandling(requested:URLRequest,session:URLSession,callback: @escaping ((Error?, [String: Any]?) -> Void))-> Void
    { 
    
    let request = requested
    let urlSession = session
    let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
        if let error = error {
            print(error.localizedDescription)
            callback(error, nil)
            return
        }
        guard let data = data else {
            return
        }
        let json = JsonParser0.serializeData(data: data)
        callback(nil, json)
        
    }
    dataTask.resume()
    }
    
}
