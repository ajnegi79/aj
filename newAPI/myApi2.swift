
import Foundation
class myApi2 {
    
    static func deleteProfile(){
        print("delete one user")
    }
    
    static func postApi(withURLRequest urlRequest: URLRequest, callback: @escaping ((Error?, [String: Any]?) -> Void)) {
        var request = urlRequest
        request.httpMethod = "GET"
        let urlSession = URLSession(configuration: .default)
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                callback(error, nil)
                return
            }
            guard let data = data else {
                return
            }
            let json = JsonParser1.serializeData(data: data)
            callback(nil, json)
            
        }
        dataTask.resume()
    }
}
