
import Foundation
class JsonParser1 {
    static func serializeData (data : Data) -> [String: Any]?{
        var json: [String: Any]?
        do {
            json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]
        } catch {
            print(error.localizedDescription)
        }
        return json
    }
}
