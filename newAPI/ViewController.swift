//
//  ViewController.swift
//  api
//
//  Created by soc-macmini-57 on 07/02/19.
//  Copyright © 2019 soc-macmini-57. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
//    var indicator: UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var myTable: UITableView!
    var array = [Currency]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        getCurrency()
    }
    
    
    func getCurrency () {
        
        
       Currency.getList { (currencies1) in
            DispatchQueue.main.async {
                self.myTable.reloadData()
            }
            self.array = currencies1
        }
        
    }
    
    @IBAction func next(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NewViewController") as? NewViewController; self.navigationController?.pushViewController(vc!, animated: true)

    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      return  self.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTable.dequeueReusableCell(withIdentifier: "cell") as? APITableViewCell
        cell?.code.text = array[indexPath.row].code
        cell?.symbol.text = array[indexPath.row].symbol
        cell?.id.text = String(array[indexPath.row].id)
        cell?.name.text = array[indexPath.row].name
        return cell!
    }
    
    
}

