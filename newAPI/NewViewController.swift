import UIKit
import NVActivityIndicatorView
var flag = true

class NewViewController: UIViewController, NVActivityIndicatorViewable {

    var indicator: UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var newTable: UITableView!
    var newArr = [Profile]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      getProfile()
    }
    func getProfile() {
        if flag {
            startAnimating(message: "Please wait")
        }
        else {
            newTable.reloadData()
        }
        flag = false
        Profile.getAnotherList { (profiles) in
            DispatchQueue.main.async {
                self.newTable.reloadData()
            }
          self.stopAnimating()
            self.newArr = profiles
        }
    }
    }

extension NewViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell1 = newTable.dequeueReusableCell(withIdentifier: "cell1") as? NewTableViewCell
        cell1?.profileLBL.text = newArr[indexPath.row].name
        let url = NSURL(string: newArr[indexPath.row].image)
        let imageData = NSData(contentsOf: url! as URL)
        cell1?.userImage.image = UIImage(data: imageData! as Data)
      
        return cell1!
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            newArr.remove(at: indexPath.row)
            startAnimating(message: "deleting...")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute:{
                self.stopAnimating()
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
                
            })
           
          
        }
    }
   


}
