//
//  APITableViewCell.swift
//  api
//
//  Created by soc-macmini-57 on 07/02/19.
//  Copyright © 2019 soc-macmini-57. All rights reserved.
//

import UIKit

class APITableViewCell: UITableViewCell {
  
    @IBOutlet weak var code: UILabel!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var symbol: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
