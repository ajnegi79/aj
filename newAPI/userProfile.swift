
import Foundation

struct Profile {
    var name: String
    var image: String
   
}
extension Profile {
    init?(with json: [String: Any]) {
     
        guard let name = json["name"] as? String else {
            return nil
        }
        guard let image = json["image"] as? String else {
                return nil
        }
    
        self.name = name
            self.image = image
    }
}
extension Profile {
    
    static func getAnotherList(callback: @escaping (([Profile]) -> Void)) {
        
        guard  let url = URL(string: "https://sd2-hiring.herokuapp.com/api/users?") else {
            return
        }
        
        let urlRequest = URLRequest(url: url)
        myApi2.postApi(withURLRequest: urlRequest) { (error, json) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let json = json else {
                return
                
            }
            
            var profiles = [Profile]()
            guard let Profilejson = json["data"] as? [String: Any] else {
                return
                
            }
            guard let userJson = Profilejson["users"] as? [[String: Any]] else {
                return
                
            }
            print(profiles)
            for profile in userJson {
                if let ProfileObject = Profile(with: profile) {
                    profiles.append(ProfileObject)
                }
            }
            callback(profiles)
            
            
        }
    }
}
